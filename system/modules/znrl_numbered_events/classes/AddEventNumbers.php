<?php

/**
 * Contao Open Source CMS
 *
 * Copyright (c) 2005-2015 Leo Feyer
 *
 * @package   ZnrlNumberedEvents
 * @author    Lorenz Ketterer <lorenz.ketterer@web.de>
 * @license   GNU/LGPL
 * @copyright Lorenz Ketterer 2015
 */


/**
 * Namespace
 */
namespace Znrl\ZnrlNumberedEvents;

use Contao\DataContainer;
use Contao\Database;
use Contao\CalendarModel;
/**
 * Class AddEventNumbers
 *
 * Add counting numbers to events.
 * @copyright  Lorenrz Ketterer 2016
 * @author     Lorenz Ketterer <lorenz.ketterer@web.de>
 */

class AddEventNumbers extends \Backend{

    /**
     * The ID of the Calendar we want to add numbers (PID from Calendar Event where Callback was triggered)
     * @var string
     */
    public $calId;

    /**
    * Function gets triggert by onsubmit_callback in tl_calendar_files and numbers are added
    * There is no special case for an identical startTime sorting is done by MySql
    * @param DataContainer $dc
    */
    public function addEventNumbers(DataContainer $dc) {

        $this->calId = $dc->activeRecord->pid;
        $calendar = CalendarModel::findBy('id', $this->calId);

        if ($calendar->znrl_add_counting_numbers == 1) {
            $this->Database->prepare("SET @counter:=?")->execute(0);
            $this->Database->prepare("UPDATE tl_calendar_events SET znrl_counting_number=@counter:=@counter+1 WHERE pid=? AND published=? ORDER BY startTime ASC")->execute($this->calId, 1);
        }
    }
}