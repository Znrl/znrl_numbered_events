<?php

/**
 * Contao Open Source CMS
 *
 * Copyright (c) 2005-2014 Leo Feyer
 *
 * @package   ZnrlNumberedEvents
 * @author    Lorenz Ketterer <lorenz.ketterer@web.de>
 * @license   GNU/LGPL
 * @copyright Lorenz Ketterer 2015
 */


/**
 * Fields
 */
$GLOBALS['TL_LANG']['tl_calendar']['znrl_add_counting_numbers'] = array('Numbered Event', 'Events will get numbers which can be diplayed with an edited template.');