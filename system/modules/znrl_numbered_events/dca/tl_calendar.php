<?php

/**
 * Contao Open Source CMS
 *
 * Copyright (c) 2005-2015 Leo Feyer
 *
 * @package   ZnrlNumberedEvents
 * @author    Lorenz Ketterer <lorenz.ketterer@web.de>
 * @license   GNU/LGPL
 * @copyright Lorenz Ketterer 2016
 */


/**
 * Table tl_calendar
 */
$GLOBALS['TL_DCA']['tl_calendar']['palettes']['default'] = str_replace
(
    'jumpTo',
    'jumpTo,znrl_add_counting_numbers',
    $GLOBALS['TL_DCA']['tl_calendar']['palettes']['default']
    );

$GLOBALS['TL_DCA']['tl_calendar']['fields']['znrl_add_counting_numbers'] = array
(
    'label'     => &$GLOBALS['TL_LANG']['tl_calendar']['znrl_add_counting_numbers'],
    'exclude'   => true,
    'inputType' => 'checkbox',
    'eval'      => array('tl_class' => 'w50'),
    'sql'       => "char(1) NOT NULL default ''"
);