<?php

/**
 * Contao Open Source CMS
 *
 * Copyright (c) 2005-2015 Leo Feyer
 *
 * @package   ZnrlNumberedEvents
 * @author    Lorenz Ketterer <lorenz.ketterer@web.de>
 * @license   GNU/LGPL
 * @copyright Lorenz Ketterer 2016
 */


/**
 * Table tl_calendar_events
 */

$GLOBALS['TL_DCA']['tl_calendar_events']['fields']['znrl_counting_number'] = array
(
    'label'     => &$GLOBALS['TL_LANG']['tl_calendar_events']['znrl_counting_number'],
    'exclude'   => true,
    'inputType' => 'text',
    'eval'      => array('rgxp'=>'natural', 'maxlength'=>8),
    'sql'       => "varchar(8) NOT NULL default ''"
);

$GLOBALS['TL_DCA']['tl_calendar_events']['config']['onsubmit_callback'][] = array('Znrl\ZnrlNumberedEvents\AddEventNumbers', 'addEventNumbers');