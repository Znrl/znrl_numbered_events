<?php

/**
 * Contao Open Source CMS
 *
 * Copyright (c) 2005-2015 Leo Feyer
 *
 * @package ZnrlNumberedEvents
 * @link    https://contao.org
 * @license http://www.gnu.org/licenses/lgpl-3.0.html LGPL
 */


/**
 * Register the namespaces
 */
ClassLoader::addNamespaces(array
(
    'Znrl',
));


/**
 * Register the classes
 */
ClassLoader::addClasses(array
(
    // Classes
    'Znrl\ZnrlNumberedEvents\AddEventNumbers'       => 'system/modules/znrl_numbered_events/classes/AddEventNumbers.php',
));
