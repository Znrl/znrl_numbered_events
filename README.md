# README #

### What is it? ###

This is a Contao extension to add counting numbers to all published events of a contao calendar

* This can be activated in calendar settings.
* It is numbered by startTime ASC which means earliest event will have 1, latest will have the highest number.
* It is up to MySql to sort events with the same startTime (no way to chose behavior right now).
* Events will always be renumered if one event of the calendar gets saved.
This means to be up to date after deleting or moving events you should open a single event and click save/save and close.
* To show these number in Frontend use $this->znrl_counting_number in your event(list/reader) templates